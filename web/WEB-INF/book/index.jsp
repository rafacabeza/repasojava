<%-- 
    Document   : index
    Created on : 05-jun-2017, 19:52:38
    Author     : usuario
--%>

<%@page import="java.util.Iterator"%>
<%@page import="Model.Book"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <jsp:useBean id="bookList" scope="request" class="java.util.ArrayList" />

    </head>
    <body>
        <h1>Lista de libros</h1>
        <p><a href="<%= request.getContextPath()%>/book/create">Nuevo</a></p>

        <table>
            <tr>
                <th>Id</th>
                <th>Titulo</th>
                <th>Editorial</th>
                <th>Año</th>
                <th>Páginas</th>
                <th>Acciones</th>
            </tr>
            <%
                Iterator<Model.Book> iterator = bookList.iterator();
                while (iterator.hasNext()) {
                    Book item = iterator.next();%>
            <tr>
                <td><%= item.getId()%></td>
                <td><%= item.getTitle()%></td>
                <td><%= item.getPublisher()%></td>
                <td><%= item.getYear()%></td>
                <td><%= item.getPages()%></td>
                <td>
                    <a href="<%= request.getContextPath()%>/book/show/<%= item.getId() %>">Ver</a> 
                    <a href="<%= request.getContextPath()%>/book/delete/<%= item.getId() %>">Borrar</a> 
                    <a href="<%= request.getContextPath()%>/book/edit/<%= item.getId() %>">Editar</a> 
                </td>
            </tr>
            <%
                }
            %>        
        </table>

    </body>
</html>
