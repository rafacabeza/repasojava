<%-- 
    Document   : leerForm
    Created on : 29-may-2017, 19:30:46
    Author     : usuario
--%>

<%@page import="Model.Person"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        
        <jsp:useBean id="person" scope="request" class="Person" />
        <jsp:useBean id="ultimo" scope="session" class="String" />
        
        <h1>Lectura del formulario!</h1>
        <ul>
            <li><%= person.getName() %></li>        
            <li><%= person.getSurname() %></li>        
            <li>Hobbies
                <ul>
                </ul>
            </li>
        </ul>
            
        <h3>Último: </h3>
        <%= ultimo %>
    </body>
</html>
